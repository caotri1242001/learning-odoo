# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Pets(models.Model):
    _name='co.pets'
    _description= 'Pet'

    name=fields.Char(string='Name', required=True)
    nickname = fields.Char(string='Nickname', required=True)
    description = fields.Text(string='Description', required=True)
    age = fields.Integer(string='Age', required=True)
    weight = fields.Float(string='Weight (kg)', required=True)