# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    _description = "SaleOrder"

    so_description = fields.Char('Description')

    def create_data(self):
        new_order = self.env['sale.order'].sudo().create({
            'partner_id': self.partner_id.id,
            'so_description': 'New Sale Order',
            'order_line': [(0, 0, {'product_id': product.id, 'product_uom_qty': 1}) for product in self.order_line.mapped('product_id')],
        })

    def update_data(self):
        new_partner = self.env['res.partner'].sudo().search([('id', '=', self.partner_id.id)], limit=1)
        if new_partner:
                new_partner.write({
                    'name': 'New Customer Name',
                    
                })
                print("Customer updated successfully.")
        else:
            print("Customer not found.")
        
    def delete_record(self):
        order = self.env['sale.order'].sudo().browse(self.id)
        if order:
            order.unlink()
            print("Order deleted successfully.")
        else:
            print("Order not found.")