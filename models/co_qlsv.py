# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from dateutil.relativedelta import relativedelta


class Qlsv(models.Model):
    _name = 'co.qlsvs'
    _description ='Quan Ly Sinh Vien'

    mssv = fields.Char(string='Mssv', required=True)
    name = fields.Char(string='Name', required=True)
    birthday = fields.Date(string='Birthday', required=True)
    age = fields.Integer(string='Age', compute ='_compute_age', store=True)
    address = fields.Char(string='Address', required=True)
    phone = fields.Char(string='Phone', required=True)
    year_school = fields.Char(string='Nien Khoa', required=True)
    type = fields.Selection([('daihoc','Dai Hoc'),('caodang', 'Cao Dang'),('banghai', 'Bang Hai')], string= 'Loai Hinh',default='daihoc')
    major = fields.Selection([('cntt','Cong Nghe Thong Tin'),('kt','Kinh Te'),('l','Luat')], string='Chuyen Nganh',default = 'cntt')
    bang1 = fields.Char(string='bang1')
    cong_tac = fields.Text('Cong tac')

    @api.depends('birthday')
    def _compute_age(self):
        today = datetime.today().date()
        for record in self:
            if record.birthday:
                birth_date_str = record.birthday.strftime('%Y-%m-%d')
                birth_date = datetime.strptime(birth_date_str,'%Y-%m-%d').date()
                delta = relativedelta(today, birth_date)
                record.age = delta.years
            else:
                record.age = 0

    def test(self):
        pass

    
